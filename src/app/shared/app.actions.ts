import { IFilter, IHotel } from '../models';

/**
 * Get hotels action with optional payload as a filter object
 */
export class GetHotels {
  static readonly type = '[app] get hotels';
  constructor(public payload?: IFilter) { }
}

/**
 * Set hotels action with required payload as a Hotel object array
 */
export class SetHotels {
  static readonly type = '[app] set hotels';
  constructor(public payload: IHotel[]) { }
}
