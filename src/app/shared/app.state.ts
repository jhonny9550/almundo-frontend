import { IHotel, IFilter } from '../models';
import { State, Action, StateContext } from '@ngxs/store';
import { GetHotels, SetHotels } from './app.actions';
import { HotelsService } from '../services/hotels.service';
import { map, catchError } from 'rxjs/operators';

// App state interface
export interface AppStateModel {
  hotels: IHotel[];
  filters?: IFilter[];
  loading: boolean;
  error?: string;
}

// State decorator inits default state and state identifier
@State<AppStateModel>({
  name: 'app',
  defaults: {
    hotels: [],
    filters: [],
    loading: false,
    error: null
  }
})
export class AppState {
  constructor(private hotelsService: HotelsService) { }
  @Action(SetHotels)
  setHotels({ setState, getState }: StateContext<AppStateModel>, { payload }: SetHotels) {
    const currentFilters = getState().filters; // Get current state filters
    setState({ hotels: payload, loading: false, filters: currentFilters }); // Set new state
  }
  @Action(GetHotels)
  getHotels({ dispatch, patchState, getState, setState }: StateContext<AppStateModel>, { payload }: GetHotels) {
    const currentFilters = getState().filters; // Get current state filters
    let newFilters = []; // Init new filters
    if (payload) {
      newFilters = currentFilters
        .filter(el => el.field !== payload.field)
        .concat([payload])
        .filter(el => el.value && el.value !== ''); // Parse filters to remove void values and replace same fields
    } else {
      newFilters = currentFilters; // If not new filters set the current state filters
    }
    patchState({ loading: true }); // Set loading state to true
    return this
      .hotelsService
      .getHotels(newFilters)
      .pipe(map(hotels => dispatch(new SetHotels(hotels))), catchError(err => {
        setState({ hotels: [], loading: false, filters: [], error: 'Ha ocurrido un error en el servidor, por favor intentalo de nuevo.' });
        return [];
      })); // Do a request, if success dispatch a request, if catch an error, then, remove state props and set the error to new state
  }
}
