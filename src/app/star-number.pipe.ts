import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'starNumber' })
export class StarNumberPipe implements PipeTransform {

  /**
   * Return array with the same length of the input value
   * @param value Stars number input
   */
  transform(value: number): string[] {
    const array = [];
    for (let i = 0; i < value; i++) {
      array.push(i);
    }
    return array;
  }

}
