import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { IFilter } from '../models';
import { tap } from 'rxjs/operators';

@Injectable()
export class HotelsService {

  constructor(public http: HttpClient) { }

  /**
   * Parse filter objects and return the request
   * @param filters Filter array for the request
   */
  getHotels(filters: IFilter[] = []) {
    const queryParams = this.parseFilters(filters);
    let path = '';
    if (queryParams !== '') { // If there is query params set de question mark character
      path = `/v1/hotels?${queryParams}`;
    } else {
      path = `/v1/hotels`;
    }
    return this.http
      .get(`${environment.api_url}${path}`)
      .pipe(tap((res: any) => console.log('Server response: ', res))); // Return the request and log the response
  }

  /**
   * Transform filter objects to query string
   * @param filters Filter object array
   */
  private parseFilters(filters: IFilter[]) {
    return filters.reduce((total, next) => {
      const field = next.field;
      const value = next.value.replace(/ /g, '%20');
      return `${field}=${value}${total !== ''
        ? '&'
        : ''}`;
    }, '');
  }

}
