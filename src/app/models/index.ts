// Hotel object interface
export interface IHotel {
  id: string;
  name: string;
  price: number;
  stars: number;
  image: string;
  amenities: string[];
}

// Filter object interface
export interface IFilter {
  field: string;
  value: string;
}

// Star object interface
export interface IStar {
  id: number;
  active: boolean;
}
