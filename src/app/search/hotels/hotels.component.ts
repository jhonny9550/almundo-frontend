import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../../services/hotels.service';
import { Observable } from 'rxjs';
import { IHotel } from '../../models';
import { Store } from '@ngxs/store';
import { AppState } from '../../shared/app.state';
import { GetHotels } from '../../shared/app.actions';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {

  public state$: Observable<AppState>; // State observable to watch

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    this.state$ = this.store.select(state => state.app); // Get app state from general state
    this.store.dispatch(new GetHotels()); // Dispatch get hotels action
  }

}
