import { Component, OnInit, Input } from '@angular/core';
import { IHotel } from '../../../models';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {

  @Input() hotel: IHotel; // Hotel object input

  constructor() { }

  ngOnInit() {}

  /**
   * Handle image element errors, common error is when the src hasn't been finded, then set a placeholder image in the src attr.
   * @param e event from image template element
   */
  handleImageNotFound(e) {
    e.target.src = 'http://via.placeholder.com/350x233?text=Image%20not%20found';
  }

}
