import { Component, OnInit, OnDestroy } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  public showFilter = false; // Flag for toggle filter view
  public subscription: Subscription; // Screen size subscription by FlexLayout

  constructor(public media: ObservableMedia) { }

  ngOnInit() {
    this.watchScreenSize();
  }

  /**
   * Setting filter view to true when the screen size is bigger than md screens
   */
  watchScreenSize() {
    this.subscription = this.media.subscribe(value => {
      if (value.mqAlias === 'md' || value.mqAlias === 'lg' || value.mqAlias === 'xl') {
        this.showFilter = true;
      } else {
        this.showFilter = false;
      }
    });
  }

  /**
   * Unsubscribe from screen watch when component is destroyed
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /**
   * Switch filter view flag
   */
  toggleFilter() {
    this.showFilter = !this.showFilter;
  }

}
