import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetHotels } from '../../../shared/app.actions';
import { IFilter } from '../../../models';

@Component({
  selector: 'app-name',
  templateUrl: './name.component.html',
  styleUrls: ['./name.component.scss']
})
export class NameComponent implements OnInit {

  public searchInput = ''; // Name search input for binding
  public showName = true; // Name filter view flag

  constructor(
    private store: Store
  ) { }

  ngOnInit() { }

  /**
   * Switch name filter view flag
   */
  toggleName() {
    this.showName = !this.showName;
  }

  /**
   * Create filter object and dispatch get hotels actions
   */
  search() {
    const filter: IFilter = {
      field: 'name',
      value: this.searchInput
    }; // Create filter object
    this.store.dispatch(new GetHotels(filter)); // Dispatch get hotels action
  }

}
