import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetHotels } from '../../../shared/app.actions';
import { IFilter, IStar } from '../../../models';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {

  public all = false; // Flag for all stars
  public showStars = true; // Stars filter view flag
  public stars: IStar[]; // Star array for checkboxes

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    this.initStars();
  }

  /**
   * Instance stars object for render stars view
   */
  initStars() {
    this.stars = [
      { id: 5, active: false },
      { id: 4, active: false },
      { id: 3, active: false },
      { id: 2, active: false },
      { id: 1, active: false },
    ];
  }

  /**
   * Switch stars filter view flag
   */
  toggleStars() {
    this.showStars = !this.showStars;
  }

  /**
   * When a star checkbox has been selected proccess filter and dispatch action
   */
  starChange() {
    const filter: IFilter = { field: 'stars', value: null }; // Create filter object
    if (this.all) {
      filter.value = '5,4,3,2,1'; // If all stars checkbox has selected, put default filter value for all stars
    } else {
      filter.value = this.parseStars(); // Call parseStars function to get a right filter value
    }
    return this.store.dispatch(new GetHotels(filter)); // Dispatch get hotels action with filter payload
  }

  /**
   * Parse stars to query string separate by comma
   */
  parseStars() {
    return this.stars
      .filter(el => el.active)
      .reduce((total, next, index) => {
        if (total === '') {
          return '' + next.id;
        } else {
          return total + ',' + next.id;
        }
      }, '');
  }

}
