import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SearchComponent } from './search/search.component';
import { FilterComponent } from './search/filter/filter.component';
import { HotelsComponent } from './search/hotels/hotels.component';
import { NameComponent } from './search/filter/name/name.component';
import { StarsComponent } from './search/filter/stars/stars.component';
import { StarNumberPipe } from './star-number.pipe';
import { HotelComponent } from './search/hotels/hotel/hotel.component';
import { HotelsService } from './services/hotels.service';
import { AmenitieComponent } from './search/hotels/hotel/amenitie/amenitie.component';
import { PriceComponent } from './search/hotels/hotel/price/price.component';
import { AppState } from './shared/app.state';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SearchComponent,
    FilterComponent,
    HotelsComponent,
    NameComponent,
    StarsComponent,
    StarNumberPipe,
    HotelComponent,
    AmenitieComponent,
    PriceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    FlexLayoutModule,
    NgxsModule.forRoot([AppState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot()
  ],
  providers: [HotelsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
