# Prueba almundo

Prueba realizada en el framework Angular en su versión 6.0.0 y NGXS como capa de datos.

En la app responsive se puede ver el listado de hoteles y filtrarlo por nombre y número de estrellas.

### Desktop
![Desktop](./gif/desktop.gif)

### Mobile
![Mobile](./gif/mobile.gif)

# Emular

Clonar el proyecto del repositorio fuente en github.
Ejecutando el comando.

    $ git clone https://github.com/jhonny9550/almundo-frontend.git

Ingresar a la carpeta e instalar las dependencias del proyecto.

    $ cd almundo-frontend
    $ npm install

## Emular
Dentro del directorio del proyecto ejecutar

    npm start

## Compilar

Para entorno de producción

    npm run build:prod

Para entorno de laboratorio o desarrollo

    npm run build

*Autor:* Jhonny Martínez

*Email:* jhonny9550@gmail.com

*Github:* github.com/jhonny9550